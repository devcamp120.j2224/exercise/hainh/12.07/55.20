package com.example.demo;

import java.util.ArrayList;


// menu list là trả ra 1 mảng combo menu cụ thể
public class MenuList {
    public class CMenuList {
        ArrayList<Menu> menus = new ArrayList<Menu>();
    
        public ArrayList<Menu> getMenus() {
            return menus;
        }
    
        public void setMenus(ArrayList<Menu> menus) {
            this.menus = menus;
        }
    
        @Override
        public String toString() {
            return "CMenuList {menus=" + menus + "}";
        }
    }
}
