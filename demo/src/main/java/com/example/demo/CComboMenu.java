package com.example.demo;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class CComboMenu {
    @CrossOrigin
	@GetMapping("/combomenu")
	public ArrayList<Menu> getComboMenu(){
		// tạo arraylist cho class Menu và truyền tham số vào 
		ArrayList<Menu> menu = new ArrayList<Menu>();
		Menu sizeS = new Menu('S', 20, 2, 200, 2, 150000);
		Menu sizeM = new Menu('M', 25, 4, 300, 3, 200000);
		Menu sizeL = new Menu('L', 30, 8, 500, 4, 250000);
		
		sizeL.setDonGia(200000);
		
		menu.add(sizeS);
		menu.add(sizeM);
		menu.add(sizeL);
		
		return menu;
	}

	@CrossOrigin
	@GetMapping("/combomenuitem")
	public Menu getComboMenuItem(){
		
		ArrayList<Menu> menu = new ArrayList<Menu>();
		Menu sizeS = new Menu('S', 20, 2, 200, 2, 150000);
		Menu sizeM = new Menu('M', 25, 4, 300, 3, 200000);
		Menu sizeL = new Menu('L', 30, 8, 500, 4, 250000);
		
		sizeL.setDonGia(200000);
		
		menu.add(sizeS);
		menu.add(sizeM);
		menu.add(sizeL);
		
		return sizeL;
	}	

    
}
